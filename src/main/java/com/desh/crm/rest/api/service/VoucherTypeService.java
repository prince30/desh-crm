package com.desh.crm.rest.api.service;

import java.util.List;
import com.desh.crm.rest.api.dto.VoucherTypeDto;

public interface VoucherTypeService {

	VoucherTypeDto create(VoucherTypeDto voucherTypeDto);

	VoucherTypeDto fetchById(Long voucherTypeId);

	VoucherTypeDto update(Long voucherTypeId, VoucherTypeDto voucherTypeDto);

	void delete(Long voucherTypeId);

	List<VoucherTypeDto> fetchAll(int page, int limit);

}
