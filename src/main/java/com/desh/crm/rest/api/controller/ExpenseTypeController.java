package com.desh.crm.rest.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.desh.crm.rest.api.dto.ExpenseTypeDto;
import com.desh.crm.rest.api.request.ExpenseTypeRequestData;
import com.desh.crm.rest.api.response.ExpenseTypeResponse;
import com.desh.crm.rest.api.response.OperationStatusModel;
import com.desh.crm.rest.api.response.RequestOperationStatus;
import com.desh.crm.rest.api.response.ResponseHandler;
import com.desh.crm.rest.api.service.ExpenseTypeService;

@RestController
@RequestMapping("expenseTypes") // http://localhost:9000/desh-crm/api/v1/departments
public class ExpenseTypeController {

	private ExpenseTypeService expenseTypeService;

	public ExpenseTypeController(ExpenseTypeService expenseTypeService) {
		this.expenseTypeService = expenseTypeService;
	}

//	@PostMapping
//	public ResponseEntity<ExpenseTypeResponse> store(@Valid @RequestBody ExpenseTypeRequestData expenseTypeRequestData)
//			throws Exception {
//
//		ExpenseTypeResponse returnValue = new ExpenseTypeResponse();
//
//		ModelMapper modelMapper = new ModelMapper();
//
//		ExpenseTypeDto expenseTypeDto = modelMapper.map(expenseTypeRequestData, ExpenseTypeDto.class);
//
//		ExpenseTypeDto createdExpenseType = expenseTypeService.create(expenseTypeDto);
//
//		returnValue = modelMapper.map(createdExpenseType, ExpenseTypeResponse.class);
//
//		return new ResponseEntity<ExpenseTypeResponse>(returnValue, HttpStatus.CREATED);
//	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<ExpenseTypeResponse> fetchById(@PathVariable Long id) throws Exception {

		ExpenseTypeResponse returnValue = new ExpenseTypeResponse();

		ExpenseTypeDto expenseTypeDto = expenseTypeService.fetchById(id);

		BeanUtils.copyProperties(expenseTypeDto, returnValue);

		return new ResponseEntity<ExpenseTypeResponse>(returnValue, HttpStatus.OK);
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<ExpenseTypeResponse> update(@PathVariable Long id,
			@Valid @RequestBody ExpenseTypeRequestData expenseTypeRequestData) throws Exception {

		ExpenseTypeResponse returnValue = new ExpenseTypeResponse();

		ExpenseTypeDto expenseTypeDto = new ExpenseTypeDto();

		BeanUtils.copyProperties(expenseTypeRequestData, expenseTypeDto);

		ExpenseTypeDto updateExpenseType = expenseTypeService.update(id, expenseTypeDto);

		BeanUtils.copyProperties(updateExpenseType, returnValue);

		return new ResponseEntity<ExpenseTypeResponse>(returnValue, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<OperationStatusModel> delete(@PathVariable Long id) throws Exception {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		expenseTypeService.delete(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return new ResponseEntity<OperationStatusModel>(returnValue, HttpStatus.OK);
	}

//	@GetMapping
//	public ResponseEntity<List<ExpenseTypeResponse>> fetchAll(@RequestParam(value = "page", defaultValue = "0") int page,
//			@RequestParam(value = "limit", defaultValue = "5") int limit) {
//
//		List<ExpenseTypeResponse> returnValue = new ArrayList<>();
//
//		List<ExpenseTypeDto> expenseTypes = expenseTypeService.fetchAll(page, limit);
//
//		for (ExpenseTypeDto expenseTypeDto : expenseTypes) {
//			ExpenseTypeResponse expenseTypeResponseModel = new ExpenseTypeResponse();
//			BeanUtils.copyProperties(expenseTypeDto, expenseTypeResponseModel);
//			returnValue.add(expenseTypeResponseModel);
//		}
//
//		return new ResponseEntity<List<ExpenseTypeResponse>>(returnValue, HttpStatus.OK);
//	}

	@PostMapping
	public ResponseEntity<Object> store(@Valid @RequestBody ExpenseTypeRequestData expenseTypeRequestData)
			throws Exception {

		ExpenseTypeResponse returnValue = new ExpenseTypeResponse();

		ModelMapper modelMapper = new ModelMapper();

		ExpenseTypeDto expenseTypeDto = modelMapper.map(expenseTypeRequestData, ExpenseTypeDto.class);

		ExpenseTypeDto createdExpenseType = expenseTypeService.create(expenseTypeDto);

		returnValue = modelMapper.map(createdExpenseType, ExpenseTypeResponse.class);

		return ResponseHandler.generateResponse(HttpStatus.CREATED, true, "Successfully save data!", returnValue);

//		return new ResponseEntity<ExpenseTypeResponse>(returnValue, HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<Object> fetchAll(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<ExpenseTypeResponse> returnValue = new ArrayList<>();

		List<ExpenseTypeDto> expenseTypes = expenseTypeService.fetchAll(page, limit);

		for (ExpenseTypeDto expenseTypeDto : expenseTypes) {
			ExpenseTypeResponse expenseTypeResponseModel = new ExpenseTypeResponse();
			BeanUtils.copyProperties(expenseTypeDto, expenseTypeResponseModel);
			returnValue.add(expenseTypeResponseModel);
		}

		return ResponseHandler.generateResponse(HttpStatus.OK, true, "Successfully retrieved data!", returnValue);
	}
}
