package com.desh.crm.rest.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "notices")
public class Notice extends AbstractEntity {
	public static final long serialVersionUID = 1L;
	private String title;
	@Column(columnDefinition = "text")
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
