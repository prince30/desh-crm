package com.desh.crm.rest.api.response;

public class CategoryResponse {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
