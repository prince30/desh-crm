package com.desh.crm.rest.api.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "expense_types")
public class ExpenseType extends AbstractEntity {
	public static final long serialVersionUID = 1L;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
