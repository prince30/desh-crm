package com.desh.crm.rest.api.dto;

import java.io.Serializable;

public class NoticeDto implements Serializable {

	private static final long serialVersionUID = -4924737231852101361L;

	private String title;
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
