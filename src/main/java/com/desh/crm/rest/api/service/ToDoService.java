package com.desh.crm.rest.api.service;

import java.util.List;
import com.desh.crm.rest.api.dto.ToDoDto;

public interface ToDoService {

	ToDoDto create(ToDoDto toDoDto);

	ToDoDto fetchById(Long toDoId);

	ToDoDto update(Long toDoId, ToDoDto toDoDto);

	void delete(Long ToDoId);

	List<ToDoDto> fetchAll(int page, int limit);

}
