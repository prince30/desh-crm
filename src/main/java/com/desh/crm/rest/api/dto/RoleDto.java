package com.desh.crm.rest.api.dto;

import java.io.Serializable;

public class RoleDto implements Serializable {

	private static final long serialVersionUID = -4924737231852101361L;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
