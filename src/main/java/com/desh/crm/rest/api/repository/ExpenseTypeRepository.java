package com.desh.crm.rest.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.desh.crm.rest.api.entity.ExpenseType;

@Repository
public interface ExpenseTypeRepository extends PagingAndSortingRepository<ExpenseType, Long> {

}
