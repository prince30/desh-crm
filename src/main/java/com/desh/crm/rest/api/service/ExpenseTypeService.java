package com.desh.crm.rest.api.service;

import java.util.List;
import com.desh.crm.rest.api.dto.ExpenseTypeDto;

public interface ExpenseTypeService {

	ExpenseTypeDto create(ExpenseTypeDto expenseTypeDto);

	ExpenseTypeDto fetchById(Long expenseTypeId);

	ExpenseTypeDto update(Long expenseTypeId, ExpenseTypeDto expenseTypeDto);

	void delete(Long expenseTypeId);

	List<ExpenseTypeDto> fetchAll(int page, int limit);

}
