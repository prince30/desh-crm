package com.desh.crm.rest.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.desh.crm.rest.api.dto.CategoryDto;
import com.desh.crm.rest.api.entity.Category;
import com.desh.crm.rest.api.repository.CategoryRepository;
import com.desh.crm.rest.api.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	private CategoryRepository categoryRepository;

	public CategoryServiceImpl(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	@Override
	public CategoryDto create(CategoryDto categoryDto) {
		ModelMapper modelMapper = new ModelMapper();
		Category categoryEntity = modelMapper.map(categoryDto, Category.class);

		categoryEntity.setName(categoryDto.getName());

		Category categoryStore = categoryRepository.save(categoryEntity);
		CategoryDto returnValue = modelMapper.map(categoryStore, CategoryDto.class);

		return returnValue;
	}

	@Override
	public CategoryDto fetchById(Long categoryId) {
		CategoryDto returnValue = new CategoryDto();

		Category categoryEntity = categoryRepository.findById(categoryId).get();

		if (categoryEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(categoryEntity, returnValue);

		return returnValue;
	}

	@Override
	public CategoryDto update(Long categoryId, CategoryDto categoryDto) {
		CategoryDto returnValue = new CategoryDto();

		Category categoryEntity = categoryRepository.findById(categoryId).get();

		categoryEntity.setName(categoryDto.getName());

		Category updateCategory = categoryRepository.save(categoryEntity);

		BeanUtils.copyProperties(updateCategory, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long categoryId) {
		Category categoryEntity = categoryRepository.findById(categoryId).get();

		categoryRepository.delete(categoryEntity);
	}

	@Override
	public List<CategoryDto> fetchAll(int page, int limit) {
		List<CategoryDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<Category> categoryPage = categoryRepository.findAll(pageableRequest);
		List<Category> categories = categoryPage.getContent();

		for (Category category : categories) {
			CategoryDto categoryDto = new CategoryDto();

			BeanUtils.copyProperties(category, categoryDto);
			returnValue.add(categoryDto);
		}

		return returnValue;
	}

}
