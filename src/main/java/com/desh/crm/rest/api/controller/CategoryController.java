package com.desh.crm.rest.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.desh.crm.rest.api.dto.CategoryDto;
import com.desh.crm.rest.api.request.CategoryRequestData;
import com.desh.crm.rest.api.response.CategoryResponse;
import com.desh.crm.rest.api.response.OperationStatusModel;
import com.desh.crm.rest.api.response.RequestOperationStatus;
import com.desh.crm.rest.api.service.CategoryService;

@RestController
@RequestMapping("categories") // http://localhost:9000/desh-crm/api/v1/departments
public class CategoryController {

	private CategoryService categoryService;

	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@PostMapping
	public ResponseEntity<CategoryResponse> store(@Valid @RequestBody CategoryRequestData categoryRequestData)
			throws Exception {

		CategoryResponse returnValue = new CategoryResponse();

		ModelMapper modelMapper = new ModelMapper();

		CategoryDto categoryDto = modelMapper.map(categoryRequestData, CategoryDto.class);

		CategoryDto createdCategory = categoryService.create(categoryDto);

		returnValue = modelMapper.map(createdCategory, CategoryResponse.class);

		return new ResponseEntity<CategoryResponse>(returnValue, HttpStatus.CREATED);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<CategoryResponse> fetchById(@PathVariable Long id) throws Exception {

		CategoryResponse returnValue = new CategoryResponse();

		CategoryDto categoryDto = categoryService.fetchById(id);

		BeanUtils.copyProperties(categoryDto, returnValue);

		return new ResponseEntity<CategoryResponse>(returnValue, HttpStatus.OK);
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<CategoryResponse> update(@PathVariable Long id,
			@Valid @RequestBody CategoryRequestData categoryRequestData) throws Exception {

		CategoryResponse returnValue = new CategoryResponse();

		CategoryDto categoryDto = new CategoryDto();

		BeanUtils.copyProperties(categoryRequestData, categoryDto);

		CategoryDto updateCategory = categoryService.update(id, categoryDto);

		BeanUtils.copyProperties(updateCategory, returnValue);

		return new ResponseEntity<CategoryResponse>(returnValue, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<OperationStatusModel> delete(@PathVariable Long id) throws Exception {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		categoryService.delete(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return new ResponseEntity<OperationStatusModel>(returnValue, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<CategoryResponse>> fetchAll(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<CategoryResponse> returnValue = new ArrayList<>();

		List<CategoryDto> categories = categoryService.fetchAll(page, limit);

		for (CategoryDto categoryDto : categories) {
			CategoryResponse categoryResponseModel = new CategoryResponse();
			BeanUtils.copyProperties(categoryDto, categoryResponseModel);
			returnValue.add(categoryResponseModel);
		}

		return new ResponseEntity<List<CategoryResponse>>(returnValue, HttpStatus.OK);
	}
}
