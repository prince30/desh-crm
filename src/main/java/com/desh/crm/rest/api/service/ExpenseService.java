package com.desh.crm.rest.api.service;

import java.util.List;
import com.desh.crm.rest.api.dto.ExpenseDto;

public interface ExpenseService {

	ExpenseDto create(ExpenseDto expenseDto);

	ExpenseDto fetchById(Long expenseId);

	ExpenseDto update(Long expenseId, ExpenseDto expenseDto);

	void delete(Long expenseId);

	List<ExpenseDto> fetchAll(int page, int limit);

}
