package com.desh.crm.rest.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.desh.crm.rest.api.dto.VoucherTypeDto;
import com.desh.crm.rest.api.entity.VoucherType;
import com.desh.crm.rest.api.repository.VoucherTypeRepository;
import com.desh.crm.rest.api.service.VoucherTypeService;

@Service
public class VoucherTypeServiceImpl implements VoucherTypeService {

	private VoucherTypeRepository voucherTypeRepository;

	public VoucherTypeServiceImpl(VoucherTypeRepository voucherTypeRepository) {
		this.voucherTypeRepository = voucherTypeRepository;
	}

	@Override
	public VoucherTypeDto create(VoucherTypeDto voucherTypeDto) {
		ModelMapper modelMapper = new ModelMapper();
		VoucherType voucherTypeEntity = modelMapper.map(voucherTypeDto, VoucherType.class);

		voucherTypeEntity.setName(voucherTypeDto.getName());

		VoucherType voucherTypeStore = voucherTypeRepository.save(voucherTypeEntity);
		VoucherTypeDto returnValue = modelMapper.map(voucherTypeStore, VoucherTypeDto.class);

		return returnValue;
	}

	@Override
	public VoucherTypeDto fetchById(Long voucherTypeId) {
		VoucherTypeDto returnValue = new VoucherTypeDto();

		VoucherType voucherTypeEntity = voucherTypeRepository.findById(voucherTypeId).get();

		if (voucherTypeEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(voucherTypeEntity, returnValue);

		return returnValue;
	}

	@Override
	public VoucherTypeDto update(Long voucherTypeId, VoucherTypeDto voucherTypeDto) {
		VoucherTypeDto returnValue = new VoucherTypeDto();

		VoucherType voucherTypeEntity = voucherTypeRepository.findById(voucherTypeId).get();

		voucherTypeEntity.setName(voucherTypeDto.getName());

		VoucherType updateVoucherType = voucherTypeRepository.save(voucherTypeEntity);

		BeanUtils.copyProperties(updateVoucherType, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long voucherTypeId) {
		VoucherType voucherTypeEntity = voucherTypeRepository.findById(voucherTypeId).get();

		voucherTypeRepository.delete(voucherTypeEntity);
	}

	@Override
	public List<VoucherTypeDto> fetchAll(int page, int limit) {
		List<VoucherTypeDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<VoucherType> voucherTypePage = voucherTypeRepository.findAll(pageableRequest);
		List<VoucherType> voucherTypes = voucherTypePage.getContent();

		for (VoucherType voucherType : voucherTypes) {
			VoucherTypeDto VoucherTypeDto = new VoucherTypeDto();

			BeanUtils.copyProperties(voucherType, VoucherTypeDto);
			returnValue.add(VoucherTypeDto);
		}

		return returnValue;
	}

}
