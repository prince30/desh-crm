//package com.desh.crm.rest.api.config;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Date;
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//import com.desh.crm.rest.api.service.UserService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;
//import com.desh.crm.rest.api.request.LoginRequestData;
//import com.desh.crm.rest.api.dto.UserDto;
//
//public class _AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
//
//	public _AuthenticationFilter(AuthenticationManager authenticationManager) {
//		super.setAuthenticationManager(authenticationManager);
//	}
//
//	@Override
//	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
//			throws AuthenticationException {
//		try {
//
//			LoginRequestData creds = new ObjectMapper().readValue(req.getInputStream(), LoginRequestData.class);
//
//			return getAuthenticationManager().authenticate(
//					new UsernamePasswordAuthenticationToken(creds.getEmail(), creds.getPassword(), new ArrayList<>()));
//
//		} catch (IOException e) {
//			throw new RuntimeException(e);
//		}
//	}
//
//	@Override
//	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
//			Authentication auth) throws IOException, ServletException {
//
//		String userName = ((User) auth.getPrincipal()).getUsername();
//
//		String token = Jwts.builder()
//						.setSubject(userName)
//						.setExpiration(new Date(System.currentTimeMillis() + _SecurityConstant.EXPIRATION_TIME))
//						.signWith(SignatureAlgorithm.HS512, _SecurityConstant.TOKEN_SECRET).compact();
//
//		res.addHeader(_SecurityConstant.HEADER_STRING, _SecurityConstant.TOKEN_PREFIX+token);
//	}
//}
