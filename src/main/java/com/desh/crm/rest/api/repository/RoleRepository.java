package com.desh.crm.rest.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.desh.crm.rest.api.entity.Role;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Long> {

}
