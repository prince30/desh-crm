package com.desh.crm.rest.api.service;

import java.util.List;
import com.desh.crm.rest.api.dto.CategoryDto;

public interface CategoryService {

	CategoryDto create(CategoryDto categoryDto);

	CategoryDto fetchById(Long categoryId);

	CategoryDto update(Long categoryId, CategoryDto categoryDto);

	void delete(Long categoryId);

	List<CategoryDto> fetchAll(int page, int limit);

}
