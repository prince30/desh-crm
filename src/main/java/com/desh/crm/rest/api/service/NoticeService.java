package com.desh.crm.rest.api.service;

import java.util.List;
import com.desh.crm.rest.api.dto.NoticeDto;

public interface NoticeService {

	NoticeDto create(NoticeDto noticeDto);

	NoticeDto fetchById(Long noticeId);

	NoticeDto update(Long noticeId, NoticeDto noticeDto);

	void delete(Long noticeId);

	List<NoticeDto> fetchAll(int page, int limit);

}
