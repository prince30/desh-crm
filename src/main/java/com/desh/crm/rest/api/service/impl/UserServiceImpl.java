//package com.desh.crm.rest.api.service.impl;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import javax.management.RuntimeErrorException;
//
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//import com.desh.crm.rest.api.dto.UserDto;
//import com.desh.crm.rest.api.entity.Role;
//import com.desh.crm.rest.api.entity.User;
//import com.desh.crm.rest.api.repository.UserRepository;
//import com.desh.crm.rest.api.service.UserService;
//
//@Service
//public class UserServiceImpl implements UserService {
//
//	private UserRepository userRepository;
//
//	@Autowired
//	private BCryptPasswordEncoder passwordEncoder;
//
//	public UserServiceImpl(UserRepository userRepository) {
//		super();
//		this.userRepository = userRepository;
//	}
//
////	@Override
////	public User create(UserDto userDto) {
////		
//////		Employee employeeEntity = new Employee();
//////		BeanUtils.copyProperties(employeeDto, employeeEntity);
//////
//////		Department departmentEntity = departmentRepository.findById(employeeDto.getDepartmentId()).get();
//////
//////		String code = uniqueCode.generateCode(4);
//////
//////		employeeEntity.setCode(code);
//////		employeeEntity.setName(employeeDto.getName());
//////		employeeEntity.setDateOfBirth(employeeDto.getDateOfBirth());
//////		employeeEntity.setDepartment(departmentEntity);
//////
//////		Employee EmployeeStore = employeeRepository.save(employeeEntity);
//////
//////		EmployeeDto returnValue = new EmployeeDto();
//////		BeanUtils.copyProperties(EmployeeStore, returnValue);
////
////		return User;
////
////
////	}
//
//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		User user = userRepository.findByEmail(username);
//
//		if (user == null) {
//			throw new UsernameNotFoundException("Invalid username and password");
//		}
//		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
//				mapRolesToAuthorities(user.getRoles()));
//	}
//
//	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
//		return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
//	}
//
//	@Override
//	public UserDto fetchById(Long userId) {
//		UserDto returnValue = new UserDto();
//
//		User userEntity = userRepository.findById(userId).get();
//
//		if (userEntity == null) {
//			throw new RuntimeErrorException(null, "ID is not found");
//		}
//		BeanUtils.copyProperties(userEntity, returnValue);
//
//		return returnValue;
//	}
//
//	@Override
//	public UserDto update(Long userId, UserDto userDto) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void delete(Long userId) {
//		User userEntity = userRepository.findById(userId).get();
//
//		userRepository.delete(userEntity);
//
//	}
//
//	@Override
//	public List<UserDto> fetchAll(int page, int limit) {
//		List<UserDto> returnValue = new ArrayList<>();
//
//		if (page > 0)
//			page = page - 1;
//
//		Pageable pageableRequest = PageRequest.of(page, limit);
//		Page<User> userPage = userRepository.findAll(pageableRequest);
//		List<User> users = userPage.getContent();
//
//		for (User User : users) {
//			UserDto userDto = new UserDto();
//			BeanUtils.copyProperties(User, userDto);
//			returnValue.add(userDto);
//		}
//
//		return returnValue;
//	}
//
//	@Override
//	public UserDto getUserByEmail(String email) {
//		User userEntity = userRepository.findByEmail(email);
//		if (userEntity == null) {
//			throw new UsernameNotFoundException(email);
//		}
//		return new ModelMapper().map(userEntity, UserDto.class);
//	}
//
//}
