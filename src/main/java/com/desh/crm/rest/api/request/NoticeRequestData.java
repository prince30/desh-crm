package com.desh.crm.rest.api.request;

import javax.validation.constraints.NotEmpty;

public class NoticeRequestData {

	@NotEmpty(message = "Name field is required")
	private String title;
	@NotEmpty(message = "Name field is required")
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
