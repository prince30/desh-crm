package com.desh.crm.rest.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.desh.crm.rest.api.dto.NoticeDto;
import com.desh.crm.rest.api.request.NoticeRequestData;
import com.desh.crm.rest.api.response.NoticeResponse;
import com.desh.crm.rest.api.response.OperationStatusModel;
import com.desh.crm.rest.api.response.RequestOperationStatus;
import com.desh.crm.rest.api.service.NoticeService;

@RestController
@RequestMapping("notices") // http://localhost:9000/desh-crm/api/v1/departments
public class NoticeController {

	private NoticeService noticeService;

	public NoticeController(NoticeService noticeService) {
		this.noticeService = noticeService;
	}

	@PostMapping
	public ResponseEntity<NoticeResponse> store(@Valid @RequestBody NoticeRequestData noticeRequestData)
			throws Exception {

		NoticeResponse returnValue = new NoticeResponse();

		ModelMapper modelMapper = new ModelMapper();

		NoticeDto NoticeDto = modelMapper.map(noticeRequestData, NoticeDto.class);

		NoticeDto createdNotice = noticeService.create(NoticeDto);

		returnValue = modelMapper.map(createdNotice, NoticeResponse.class);

		return new ResponseEntity<NoticeResponse>(returnValue, HttpStatus.CREATED);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<NoticeResponse> fetchById(@PathVariable Long id) throws Exception {

		NoticeResponse returnValue = new NoticeResponse();

		NoticeDto noticeDto = noticeService.fetchById(id);

		BeanUtils.copyProperties(noticeDto, returnValue);

		return new ResponseEntity<NoticeResponse>(returnValue, HttpStatus.OK);
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<NoticeResponse> update(@PathVariable Long id,
			@Valid @RequestBody NoticeRequestData noticeRequestData) throws Exception {

		NoticeResponse returnValue = new NoticeResponse();

		NoticeDto noticeDto = new NoticeDto();

		BeanUtils.copyProperties(noticeRequestData, noticeDto);

		NoticeDto updateNotice = noticeService.update(id, noticeDto);

		BeanUtils.copyProperties(updateNotice, returnValue);

		return new ResponseEntity<NoticeResponse>(returnValue, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<OperationStatusModel> delete(@PathVariable Long id) throws Exception {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		noticeService.delete(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return new ResponseEntity<OperationStatusModel>(returnValue, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<NoticeResponse>> fetchAll(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<NoticeResponse> returnValue = new ArrayList<>();

		List<NoticeDto> notices = noticeService.fetchAll(page, limit);

		for (NoticeDto noticeDto : notices) {
			NoticeResponse noticeResponseModel = new NoticeResponse();
			BeanUtils.copyProperties(noticeDto, noticeResponseModel);
			returnValue.add(noticeResponseModel);
		}

		return new ResponseEntity<List<NoticeResponse>>(returnValue, HttpStatus.OK);
	}
}
