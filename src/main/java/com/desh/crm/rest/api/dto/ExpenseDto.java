package com.desh.crm.rest.api.dto;

import java.io.Serializable;
import java.util.Set;

import com.desh.crm.rest.api.entity.ExpenseType;

public class ExpenseDto implements Serializable {

	private static final long serialVersionUID = -4924737231852101361L;

	private String title;
	private double amount;
//	private Set<ExpenseTypeDto> exenseTYpe;
	private ExpenseType expenseType;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public ExpenseType getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(ExpenseType expenseType) {
		this.expenseType = expenseType;
	}

}
