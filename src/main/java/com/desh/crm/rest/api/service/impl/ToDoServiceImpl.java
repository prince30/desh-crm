package com.desh.crm.rest.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.desh.crm.rest.api.dto.ToDoDto;
import com.desh.crm.rest.api.entity.ToDo;
import com.desh.crm.rest.api.repository.ToDoRepository;
import com.desh.crm.rest.api.service.ToDoService;

@Service
public class ToDoServiceImpl implements ToDoService {

	private ToDoRepository toDoRepository;

	public ToDoServiceImpl(ToDoRepository toDoRepository) {
		this.toDoRepository = toDoRepository;
	}

	@Override
	public ToDoDto create(ToDoDto toDoDto) {
		ModelMapper modelMapper = new ModelMapper();
		ToDo toDoEntity = modelMapper.map(toDoDto, ToDo.class);

		toDoEntity.setTitle(toDoDto.getTitle());
		toDoEntity.setDate(toDoDto.getDate());
		toDoEntity.setTime(toDoDto.getTime());
		toDoEntity.setPriroty(toDoDto.getPriroty());
		toDoEntity.setStatus(toDoDto.getStatus());
		toDoEntity.setNote(toDoDto.getNote());

		ToDo ToDoStore = toDoRepository.save(toDoEntity);
		ToDoDto returnValue = modelMapper.map(ToDoStore, ToDoDto.class);

		return returnValue;
	}

	@Override
	public ToDoDto fetchById(Long toDoId) {
		ToDoDto returnValue = new ToDoDto();

		ToDo toDoEntity = toDoRepository.findById(toDoId).get();

		if (toDoEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(toDoEntity, returnValue);

		return returnValue;
	}

	@Override
	public ToDoDto update(Long toDoId, ToDoDto toDoDto) {
		ToDoDto returnValue = new ToDoDto();

		ToDo toDoEntity = toDoRepository.findById(toDoId).get();

		toDoEntity.setTitle(toDoDto.getTitle());
		toDoEntity.setDate(toDoDto.getDate());
		toDoEntity.setTime(toDoDto.getTime());
		toDoEntity.setPriroty(toDoDto.getPriroty());
		toDoEntity.setStatus(toDoDto.getStatus());
		toDoEntity.setNote(toDoDto.getNote());

		ToDo updateToDo = toDoRepository.save(toDoEntity);

		BeanUtils.copyProperties(updateToDo, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long ToDoId) {
		ToDo toDoEntity = toDoRepository.findById(ToDoId).get();

		toDoRepository.delete(toDoEntity);
	}

	@Override
	public List<ToDoDto> fetchAll(int page, int limit) {
		List<ToDoDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<ToDo> toDoPage = toDoRepository.findAll(pageableRequest);
		List<ToDo> toDos = toDoPage.getContent();

		for (ToDo toDo : toDos) {
			ToDoDto toDoDto = new ToDoDto();

			BeanUtils.copyProperties(toDo, toDoDto);
			returnValue.add(toDoDto);
		}

		return returnValue;
	}

}
