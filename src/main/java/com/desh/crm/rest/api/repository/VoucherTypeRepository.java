package com.desh.crm.rest.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.desh.crm.rest.api.entity.VoucherType;

@Repository
public interface VoucherTypeRepository extends PagingAndSortingRepository<VoucherType, Long> {

}
