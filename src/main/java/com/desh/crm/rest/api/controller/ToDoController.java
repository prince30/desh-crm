package com.desh.crm.rest.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.desh.crm.rest.api.dto.ToDoDto;
import com.desh.crm.rest.api.request.ToDoRequestData;
import com.desh.crm.rest.api.response.ToDoResponse;
import com.desh.crm.rest.api.response.OperationStatusModel;
import com.desh.crm.rest.api.response.RequestOperationStatus;
import com.desh.crm.rest.api.service.ToDoService;

@RestController
@RequestMapping("todos") // http://localhost:9000/desh-crm/api/v1/departments
public class ToDoController {

	private ToDoService toDoService;

	public ToDoController(ToDoService toDoService) {
		this.toDoService = toDoService;
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<ToDoResponse> store(@Valid @RequestBody ToDoRequestData ToDoRequestData)
			throws Exception {

		ToDoResponse returnValue = new ToDoResponse();

		ModelMapper modelMapper = new ModelMapper();

		ToDoDto toDoDto = modelMapper.map(ToDoRequestData, ToDoDto.class);

		ToDoDto createdToDo = toDoService.create(toDoDto);

		returnValue = modelMapper.map(createdToDo, ToDoResponse.class);

		return new ResponseEntity<ToDoResponse>(returnValue, HttpStatus.CREATED);
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<ToDoResponse> fetchById(@PathVariable Long id) throws Exception {

		ToDoResponse returnValue = new ToDoResponse();

		ToDoDto toDoDto = toDoService.fetchById(id);

		BeanUtils.copyProperties(toDoDto, returnValue);

		return new ResponseEntity<ToDoResponse>(returnValue, HttpStatus.OK);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<ToDoResponse> update(@PathVariable Long id,
			@Valid @RequestBody ToDoRequestData toDoRequestData) throws Exception {

		ToDoResponse returnValue = new ToDoResponse();

		ToDoDto toDoDto = new ToDoDto();

		BeanUtils.copyProperties(toDoRequestData, toDoDto);

		ToDoDto updateToDo = toDoService.update(id, toDoDto);

		BeanUtils.copyProperties(updateToDo, returnValue);

		return new ResponseEntity<ToDoResponse>(returnValue, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<OperationStatusModel> delete(@PathVariable Long id) throws Exception {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		toDoService.delete(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return new ResponseEntity<OperationStatusModel>(returnValue, HttpStatus.OK);
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<ToDoResponse>> fetchAll(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<ToDoResponse> returnValue = new ArrayList<>();

		List<ToDoDto> todos = toDoService.fetchAll(page, limit);

		for (ToDoDto toDoDto : todos) {
			ToDoResponse toDoResponseModel = new ToDoResponse();
			BeanUtils.copyProperties(toDoDto, toDoResponseModel);
			returnValue.add(toDoResponseModel);
		}

		return new ResponseEntity<List<ToDoResponse>>(returnValue, HttpStatus.OK);
	}
}
