//package com.desh.crm.rest.api;
//
//import java.util.Arrays;
//import java.util.Collection;
//import javax.transaction.Transactional;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.event.EventListener;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Component;
//import com.desh.crm.rest.api.entity.Authority;
//import com.desh.crm.rest.api.entity.Role;
//import com.desh.crm.rest.api.entity.User;
//import com.desh.crm.rest.api.repository.AuthorityRepository;
//import com.desh.crm.rest.api.repository.RoleRepository;
//import com.desh.crm.rest.api.repository.UserRepository;
//
//@Component
//public class InitialUserSetUp {
//
//	private AuthorityRepository authorityRepository;
//	private RoleRepository roleRepository;
//	private UserRepository userRepository;
//
//	public InitialUserSetUp(AuthorityRepository authorityRepository, RoleRepository roleRepository, UserRepository userRepository) {
//		this.authorityRepository = authorityRepository;
//		this.roleRepository = roleRepository;
//		this.userRepository = userRepository;
//	}
//	
//	@Bean
//	public BCryptPasswordEncoder passwordEncoder() {
//	    return new BCryptPasswordEncoder();
//	}
//
//	@EventListener
//	@Transactional
//	public void onApplicationEvent(ApplicationReadyEvent event) {
//		System.out.println("ready event ...");
//
//		Authority readAuthority = createAuthority("READ_AUTHORITY");
//		Authority writeAuthority = createAuthority("WRITE_AUTHORITY");
//		Authority deleteAuthority = createAuthority("DELETE_AUTHORITY");
//		
//		Role roleUser =createRole("ROLE_USER", Arrays.asList(readAuthority, writeAuthority));
//		Role roleAdmin =createRole("ROLE_ADMIN", Arrays.asList(readAuthority, writeAuthority, deleteAuthority));
//		
//		User admin =new User();
//		admin.setName("Test");
//		admin.setPassword(passwordEncoder().encode("123456"));
//		admin.setEmail("test@gmail.com");
//		admin.setPhone("01718967534");
//		admin.setCompany("D");
//		admin.setDesignation("Developer");
//		admin.setCountry("US");
//		admin.setWebsite("test@website.com");
//		admin.setSource("Test");
//		admin.setRating("8");
//		admin.setStatus("ACTIVE");
//		admin.setBusinessHistory("Test");
//		admin.setRoles(Arrays.asList(roleAdmin));
//		
//		userRepository.save(admin);
//		
//
//	}
//
//	@Transactional
//	private Authority createAuthority(String name) {
//
//		Authority authorityEntity = new Authority(name);
//		authorityRepository.save(authorityEntity);
//
//		return authorityEntity;
//
//	}
//
//	@Transactional
//	private Role createRole(String name, Collection<Authority> authorities) {
//
//		Role roleEntity = new Role(name);
//		roleRepository.save(roleEntity);
//
//		return roleEntity;
//
//	}
//
//}
