package com.desh.crm.rest.api.service;

import java.util.List;

import com.desh.crm.rest.api.dto.RoleDto;

public interface RoleService {

	RoleDto create(RoleDto roleDto);

	RoleDto fetchById(Long roleId);

	RoleDto update(Long roleId, RoleDto roleDto);

	void delete(Long roleId);

	List<RoleDto> fetchAll(int page, int limit);

}
