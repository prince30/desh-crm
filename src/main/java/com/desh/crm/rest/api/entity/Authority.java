package com.desh.crm.rest.api.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "authorities")
public class Authority extends AbstractEntity implements Serializable{
	public static final long serialVersionUID = 1L;

	private String name;

	@ManyToMany(mappedBy = "authorities")
	private Collection<Role> roles;

	public Authority() {
		super();
	}

	public Authority(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

}
