package com.desh.crm.rest.api.config;

public class _SecurityConstant {
	public static final long EXPIRATION_TIME = 864000000;
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/users ";
	public static final String TOKEN_SECRET = "h4of9eh48vmg02nfu30v27yen295hfj65 ";
}
