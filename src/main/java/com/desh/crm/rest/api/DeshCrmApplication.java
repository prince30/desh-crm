package com.desh.crm.rest.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeshCrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeshCrmApplication.class, args);
	}

}
