package com.desh.crm.rest.api.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "expenses")
public class Expense extends AbstractEntity {
	public static final long serialVersionUID = 1L;
	
	private String title;
	private double amount;

	@OneToOne
	@JoinColumn(name = "expense_type_id")
	private ExpenseType expenseType;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public ExpenseType getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(ExpenseType expenseType) {
		this.expenseType = expenseType;
	}
}
