//package com.desh.crm.rest.api.service.impl;
//
//import java.util.ArrayList;
//import java.util.List;
//import javax.management.RuntimeErrorException;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.BeanUtils;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.stereotype.Service;
//import com.desh.crm.rest.api.dto.ExpenseDto;
//import com.desh.crm.rest.api.entity.Expense;
//import com.desh.crm.rest.api.entity.ExpenseType;
//import com.desh.crm.rest.api.repository.ExpenseRepository;
//import com.desh.crm.rest.api.service.ExpenseTypeService;
//
//@Service
//public class ExpenseServiceImpl implements ExpenseService {
//
//	private ExpenseRepository expenseRepository;
//
//	public ExpenseServiceImpl(ExpenseRepository expenseRepository) {
//		this.expenseRepository = expenseRepository;
//	}
//
//	@Override
//	public ExpenseDto create(ExpenseDto expenseDto) {
//		ModelMapper modelMapper = new ModelMapper();
//		Expense expenseEntity = modelMapper.map(expenseDto, Expense.class);
//
//		expenseTypeEntity.setName(ExpenseDto.getName());
//
//		ExpenseType expenseTypeStore = ExpenseRepository.save(expenseTypeEntity);
//		ExpenseDto returnValue = modelMapper.map(expenseTypeStore, ExpenseDto.class);
//
//		return returnValue;
//	}
//
//	@Override
//	public ExpenseDto fetchById(Long expenseTypeId) {
//		ExpenseDto returnValue = new ExpenseDto();
//
//		ExpenseType expenseTypeEntity = ExpenseRepository.findById(expenseTypeId).get();
//
//		if (expenseTypeEntity == null) {
//			throw new RuntimeErrorException(null, "ID is not found");
//		}
//		BeanUtils.copyProperties(expenseTypeEntity, returnValue);
//
//		return returnValue;
//	}
//
//
//	public ExpenseDto update(Long expenseId, ExpenseDto expenseDto) {
//		ExpenseDto returnValue = new ExpenseDto();
//
//		Expense expenseEntity = expenseRepository.findById(expenseId).get();
//
//		expenseEntity.setName(expenseDto.ge);
//
//		Expense updateExpense = expenseRepository.save(expenseEntity);
//
//		BeanUtils.copyProperties(updateExpense, returnValue);
//
//		return returnValue;
//	}
//
//	
//	public void delete(Long expenseId) {
//		Expense expenseEntity = expenseRepository.findById(expenseId).get();
//
//		expenseRepository.delete(expenseEntity);
//	}
//
//	
//	public List<ExpenseDto> fetchAll(int page, int limit) {
//		List<ExpenseDto> returnValue = new ArrayList<>();
//
//		if (page > 0)
//			page = page - 1;
//
//		Pageable pageableRequest = PageRequest.of(page, limit);
//		Page<Expense> expensePage = expenseRepository.findAll(pageableRequest);
//		List<Expense> expenses = expensePage.getContent();
//
//		for (Expense expense : expenses) {
//			ExpenseDto expenseDto = new ExpenseDto();
//
//			BeanUtils.copyProperties(expense, expenseDto);
//			returnValue.add(expenseDto);
//		}
//
//		return returnValue;
//	}
//
//}
