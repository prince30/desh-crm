package com.desh.crm.rest.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.desh.crm.rest.api.dto.VoucherTypeDto;
import com.desh.crm.rest.api.request.VoucherTypeRequestData;
import com.desh.crm.rest.api.response.VoucherTypeResponse;
import com.desh.crm.rest.api.response.OperationStatusModel;
import com.desh.crm.rest.api.response.RequestOperationStatus;
import com.desh.crm.rest.api.service.VoucherTypeService;

@RestController
@RequestMapping("voucherTypes") // http://localhost:9000/desh-crm/api/v1/departments
public class VoucherTypeController {

	private VoucherTypeService voucherTypeService;

	public VoucherTypeController(VoucherTypeService voucherTypeService) {
		this.voucherTypeService = voucherTypeService;
	}

	@PostMapping
	public ResponseEntity<VoucherTypeResponse> store(@Valid @RequestBody VoucherTypeRequestData VoucherTypeRequestData)
			throws Exception {

		VoucherTypeResponse returnValue = new VoucherTypeResponse();

		ModelMapper modelMapper = new ModelMapper();

		VoucherTypeDto voucherTypeDto = modelMapper.map(VoucherTypeRequestData, VoucherTypeDto.class);

		VoucherTypeDto createdVoucherType = voucherTypeService.create(voucherTypeDto);

		returnValue = modelMapper.map(createdVoucherType, VoucherTypeResponse.class);

		return new ResponseEntity<VoucherTypeResponse>(returnValue, HttpStatus.CREATED);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<VoucherTypeResponse> fetchById(@PathVariable Long id) throws Exception {

		VoucherTypeResponse returnValue = new VoucherTypeResponse();

		VoucherTypeDto voucherTypeDto = voucherTypeService.fetchById(id);

		BeanUtils.copyProperties(voucherTypeDto, returnValue);

		return new ResponseEntity<VoucherTypeResponse>(returnValue, HttpStatus.OK);
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<VoucherTypeResponse> update(@PathVariable Long id,
			@Valid @RequestBody VoucherTypeRequestData VoucherTypeRequestData) throws Exception {

		VoucherTypeResponse returnValue = new VoucherTypeResponse();

		VoucherTypeDto voucherTypeDto = new VoucherTypeDto();

		BeanUtils.copyProperties(VoucherTypeRequestData, voucherTypeDto);

		VoucherTypeDto updateVoucherType = voucherTypeService.update(id, voucherTypeDto);

		BeanUtils.copyProperties(updateVoucherType, returnValue);

		return new ResponseEntity<VoucherTypeResponse>(returnValue, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<OperationStatusModel> delete(@PathVariable Long id) throws Exception {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		voucherTypeService.delete(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return new ResponseEntity<OperationStatusModel>(returnValue, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<VoucherTypeResponse>> fetchAll(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<VoucherTypeResponse> returnValue = new ArrayList<>();

		List<VoucherTypeDto> voucherTypes = voucherTypeService.fetchAll(page, limit);

		for (VoucherTypeDto voucherTypeDto : voucherTypes) {
			VoucherTypeResponse VoucherTypeResponseModel = new VoucherTypeResponse();
			BeanUtils.copyProperties(voucherTypeDto, VoucherTypeResponseModel);
			returnValue.add(VoucherTypeResponseModel);
		}

		return new ResponseEntity<List<VoucherTypeResponse>>(returnValue, HttpStatus.OK);
	}
}
