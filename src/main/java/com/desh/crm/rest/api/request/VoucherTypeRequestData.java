package com.desh.crm.rest.api.request;

import javax.validation.constraints.NotEmpty;

public class VoucherTypeRequestData {

	@NotEmpty(message = "Name field is required")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
