package com.desh.crm.rest.api.request;

import javax.validation.constraints.NotEmpty;

import com.desh.crm.rest.api.entity.ExpenseType;

public class ExpenseRequestData {

	@NotEmpty(message = "Name field is required")
	private String title;
	@NotEmpty(message = "Amount field is required")
	private double amount;

	private ExpenseType expenseType;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public ExpenseType getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(ExpenseType expenseType) {
		this.expenseType = expenseType;
	}


}
