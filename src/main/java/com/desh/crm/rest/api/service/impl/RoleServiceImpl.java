package com.desh.crm.rest.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.desh.crm.rest.api.dto.RoleDto;
import com.desh.crm.rest.api.entity.Role;
import com.desh.crm.rest.api.repository.RoleRepository;
import com.desh.crm.rest.api.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	private RoleRepository roleRepository;

	public RoleServiceImpl(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Override
	public RoleDto create(RoleDto roleDto) {
		ModelMapper modelMapper = new ModelMapper();
		Role roleEntity = modelMapper.map(roleDto, Role.class);

		roleEntity.setName(roleDto.getName());

		Role roleStore = roleRepository.save(roleEntity);
		RoleDto returnValue = modelMapper.map(roleStore, RoleDto.class);

		return returnValue;
	}

	@Override
	public RoleDto fetchById(Long roleId) {
		RoleDto returnValue = new RoleDto();

		Role roleEntity = roleRepository.findById(roleId).get();

		if (roleEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(roleEntity, returnValue);

		return returnValue;
	}

	@Override
	public RoleDto update(Long roleId, RoleDto roleDto) {
		RoleDto returnValue = new RoleDto();

		Role roleEntity = roleRepository.findById(roleId).get();

		roleEntity.setName(roleDto.getName());

		Role updateRole = roleRepository.save(roleEntity);

		BeanUtils.copyProperties(updateRole, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long roleId) {
		Role roleEntity = roleRepository.findById(roleId).get();

		roleRepository.delete(roleEntity);
	}

	@Override
	public List<RoleDto> fetchAll(int page, int limit) {
		List<RoleDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<Role> rolePage = roleRepository.findAll(pageableRequest);
		List<Role> roles = rolePage.getContent();

		for (Role role : roles) {
			RoleDto roleDto = new RoleDto();

			BeanUtils.copyProperties(role, roleDto);
			returnValue.add(roleDto);
		}

		return returnValue;
	}

}
