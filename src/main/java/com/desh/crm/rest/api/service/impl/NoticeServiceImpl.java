package com.desh.crm.rest.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.desh.crm.rest.api.dto.NoticeDto;
import com.desh.crm.rest.api.entity.Notice;
import com.desh.crm.rest.api.repository.NoticeRepository;
import com.desh.crm.rest.api.service.NoticeService;

@Service
public class NoticeServiceImpl implements NoticeService {

	private NoticeRepository noticeRepository;

	public NoticeServiceImpl(NoticeRepository noticeRepository) {
		this.noticeRepository = noticeRepository;
	}

	@Override
	public NoticeDto create(NoticeDto noticeDto) {
		ModelMapper modelMapper = new ModelMapper();
		Notice noticeEntity = modelMapper.map(noticeDto, Notice.class);

		noticeEntity.setTitle(noticeDto.getTitle());
		noticeEntity.setDescription(noticeDto.getDescription());

		Notice noticeStore = noticeRepository.save(noticeEntity);
		NoticeDto returnValue = modelMapper.map(noticeStore, NoticeDto.class);

		return returnValue;
	}

	@Override
	public NoticeDto fetchById(Long noticeId) {
		NoticeDto returnValue = new NoticeDto();

		Notice noticeEntity = noticeRepository.findById(noticeId).get();

		if (noticeEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(noticeEntity, returnValue);

		return returnValue;
	}

	@Override
	public NoticeDto update(Long noticeId, NoticeDto noticeDto) {
		NoticeDto returnValue = new NoticeDto();

		Notice noticeEntity = noticeRepository.findById(noticeId).get();

		noticeEntity.setTitle(noticeDto.getTitle());
		noticeEntity.setDescription(noticeDto.getDescription());

		Notice updateNotice = noticeRepository.save(noticeEntity);

		BeanUtils.copyProperties(updateNotice, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long noticeId) {
		Notice noticeEntity = noticeRepository.findById(noticeId).get();

		noticeRepository.delete(noticeEntity);
	}

	@Override
	public List<NoticeDto> fetchAll(int page, int limit) {
		List<NoticeDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<Notice> noticePage = noticeRepository.findAll(pageableRequest);
		List<Notice> notices = noticePage.getContent();

		for (Notice notice : notices) {
			NoticeDto noticeDto = new NoticeDto();

			BeanUtils.copyProperties(notice, noticeDto);
			returnValue.add(noticeDto);
		}

		return returnValue;
	}

}
