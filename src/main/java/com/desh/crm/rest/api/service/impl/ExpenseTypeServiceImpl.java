package com.desh.crm.rest.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.desh.crm.rest.api.dto.ExpenseTypeDto;
import com.desh.crm.rest.api.entity.ExpenseType;
import com.desh.crm.rest.api.repository.ExpenseTypeRepository;
import com.desh.crm.rest.api.service.ExpenseTypeService;

@Service
public class ExpenseTypeServiceImpl implements ExpenseTypeService {

	private ExpenseTypeRepository expenseTypeRepository;

	public ExpenseTypeServiceImpl(ExpenseTypeRepository expenseTypeRepository) {
		this.expenseTypeRepository = expenseTypeRepository;
	}

	@Override
	public ExpenseTypeDto create(ExpenseTypeDto expenseTypeDto) {
		ModelMapper modelMapper = new ModelMapper();
		ExpenseType expenseTypeEntity = modelMapper.map(expenseTypeDto, ExpenseType.class);

		expenseTypeEntity.setName(expenseTypeDto.getName());

		ExpenseType expenseTypeStore = expenseTypeRepository.save(expenseTypeEntity);
		ExpenseTypeDto returnValue = modelMapper.map(expenseTypeStore, ExpenseTypeDto.class);

		return returnValue;
	}

	@Override
	public ExpenseTypeDto fetchById(Long expenseTypeId) {
		ExpenseTypeDto returnValue = new ExpenseTypeDto();

		ExpenseType expenseTypeEntity = expenseTypeRepository.findById(expenseTypeId).get();

		if (expenseTypeEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(expenseTypeEntity, returnValue);

		return returnValue;
	}

	@Override
	public ExpenseTypeDto update(Long expenseTypeId, ExpenseTypeDto expenseTypeDto) {
		ExpenseTypeDto returnValue = new ExpenseTypeDto();

		ExpenseType expenseTypeEntity = expenseTypeRepository.findById(expenseTypeId).get();

		expenseTypeEntity.setName(expenseTypeDto.getName());

		ExpenseType updateExpenseType = expenseTypeRepository.save(expenseTypeEntity);

		BeanUtils.copyProperties(updateExpenseType, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long expenseTypeId) {
		ExpenseType expenseTypeEntity = expenseTypeRepository.findById(expenseTypeId).get();

		expenseTypeRepository.delete(expenseTypeEntity);
	}

	@Override
	public List<ExpenseTypeDto> fetchAll(int page, int limit) {
		List<ExpenseTypeDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<ExpenseType> ExpenseTypePage = expenseTypeRepository.findAll(pageableRequest);
		List<ExpenseType> expenseTypes = ExpenseTypePage.getContent();

		for (ExpenseType expenseType : expenseTypes) {
			ExpenseTypeDto expenseTypeDto = new ExpenseTypeDto();

			BeanUtils.copyProperties(expenseType, expenseTypeDto);
			returnValue.add(expenseTypeDto);
		}

		return returnValue;
	}

}
