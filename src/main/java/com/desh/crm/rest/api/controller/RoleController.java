package com.desh.crm.rest.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.desh.crm.rest.api.dto.RoleDto;
import com.desh.crm.rest.api.request.RoleRequestData;
import com.desh.crm.rest.api.response.RoleResponse;
import com.desh.crm.rest.api.response.OperationStatusModel;
import com.desh.crm.rest.api.response.RequestOperationStatus;
import com.desh.crm.rest.api.service.RoleService;

@RestController
@RequestMapping("roles") // http://localhost:9000/desh-crm/api/v1/departments
public class RoleController {

	private RoleService roleService;

	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}

	@PostMapping
	public ResponseEntity<RoleResponse> store(@Valid @RequestBody RoleRequestData roleRequestData)
			throws Exception {

		RoleResponse returnValue = new RoleResponse();

		ModelMapper modelMapper = new ModelMapper();

		RoleDto roleDto = modelMapper.map(roleRequestData, RoleDto.class);

		RoleDto createdRole = roleService.create(roleDto);

		returnValue = modelMapper.map(createdRole, RoleResponse.class);

		return new ResponseEntity<RoleResponse>(returnValue, HttpStatus.CREATED);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<RoleResponse> fetchById(@PathVariable Long id) throws Exception {

		RoleResponse returnValue = new RoleResponse();

		RoleDto roleDto = roleService.fetchById(id);

		BeanUtils.copyProperties(roleDto, returnValue);

		return new ResponseEntity<RoleResponse>(returnValue, HttpStatus.OK);
	}

	@PutMapping(path = "/{id}")
	public ResponseEntity<RoleResponse> update(@PathVariable Long id,
			@Valid @RequestBody RoleRequestData roleRequestData) throws Exception {

		RoleResponse returnValue = new RoleResponse();

		RoleDto roleDto = new RoleDto();

		BeanUtils.copyProperties(roleRequestData, roleDto);

		RoleDto updateRole = roleService.update(id, roleDto);

		BeanUtils.copyProperties(updateRole, returnValue);

		return new ResponseEntity<RoleResponse>(returnValue, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<OperationStatusModel> delete(@PathVariable Long id) throws Exception {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		roleService.delete(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return new ResponseEntity<OperationStatusModel>(returnValue, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<RoleResponse>> fetchAll(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<RoleResponse> returnValue = new ArrayList<>();

		List<RoleDto> roles = roleService.fetchAll(page, limit);

		for (RoleDto roleDto : roles) {
			RoleResponse RoleResponseModel = new RoleResponse();
			BeanUtils.copyProperties(roleDto, RoleResponseModel);
			returnValue.add(RoleResponseModel);
		}

		return new ResponseEntity<List<RoleResponse>>(returnValue, HttpStatus.OK);
	}
}
