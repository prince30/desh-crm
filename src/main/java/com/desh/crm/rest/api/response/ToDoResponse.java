package com.desh.crm.rest.api.response;

import java.util.Date;

public class ToDoResponse {

	private String title;
	private Date date;
	private String time;
	private String priroty;
	private String status;
	private String note;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getPriroty() {
		return priroty;
	}

	public void setPriroty(String priroty) {
		this.priroty = priroty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
